import axios from "axios";

const FlightPlanApi = {
    getAllFlightPlan: () => axios.get("http://localhost:8080/flightplans"),
    getAllflightPlanOfAUser: (userId : number) => axios.get("http://localhost:8080/flightplans?userId="+userId.toString()),
    getAFlightPlan: (userId : number, number : number) => axios.get("http://localhost:8080/flightplans/"+userId.toString()+"/"+number.toString()),
    deleteFlightPlan: (userId : number, number : number) => axios.delete("http://localhost:8080/flightplans/"+userId.toString()+"/"+number.toString()),
}

export default FlightPlanApi;