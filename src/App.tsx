import { useEffect } from 'react';
import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import HomePage from "./pages/HomePage";
import FlightPlanPage from './pages/FlightPlanPage';
import ChatPage from './pages/ChatPage';
import HelpPage from './pages/HelpPage';
import FlightPlanDetailsPage from './pages/FlightPlanDetailsPage';

function App() {
  useEffect(() => {
    document.title = 'Aeroplanner';
  }, []);
 
  return (
    <Router>
      <Routes>
        <Route path="/" element={<HomePage/>} />
        <Route path="/flightplans" element={<FlightPlanPage/>} />
        <Route path="/flightplans/:userId" element={<FlightPlanPage/>} />
        <Route path="/chat" element={<ChatPage/>} />
        <Route path="/help" element={<HelpPage/>} />
        <Route path="/flightplan/:userId/:number" element={<FlightPlanDetailsPage/>} />
      </Routes>
    </Router>
  )
}

export default App;
