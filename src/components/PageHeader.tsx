import { IconType } from "react-icons";

interface Props {
    title: string;
    icon: IconType;
    bgImage: string | undefined;
}

function PageHeader(props: Props){
    const styles = {
        backgroundImage: `url("${props.bgImage}")`,
    };
    return(
        <div className="w-full bg-cover bg-no-repeat " style={styles}>
            <div className="flex flex-row space-x-5 items-center w-full p-4 bg-gradient-to-t from-zinc-900 to-transparent">
                <props.icon size="4rem" className="text-white drop-shadow"/>
                <h1 className="text-[4rem] font-bold text-white drop-shadow">{props.title}</h1>
            </div>
        </div>
    )
}

export default PageHeader;