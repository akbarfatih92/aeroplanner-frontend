//import React from "react";
import {Link, NavLink} from 'react-router-dom';
import LogoBanner from '../assets/logo_banner.png';
import PlaceholderUser from '../assets/user_default.png';
import { BiSolidBell } from 'react-icons/bi';

function Navbar() {
  const links = [
    {
      id: 1,
      path: "/",
      text: "Dashboard",
    },
    {
      id: 2,
      path: "/flightplans",
      text: "Flight Plans",
    },
    {
      id: 3,
      path: "/chat",
      text: "Chat With ATC",
    },
    {
      id: 4,
      path: "/help",
      text: "Help",
    },
  ];

  return (
    <nav className="border-gray-200 bg-zinc-900 sticky top-0 z-50 w-full z-100">
      <div className="flex flex-wrap items-center justify-between mx-auto py-2 px-6">
        <Link to="/" className="flex items-center">
          <img
            src={LogoBanner}
            className="h-8 mr-3"
            alt="Flowbite Logo"
          />
        </Link>
        <div className="w-max grow" id="navbar-default">
          <ul className="font-medium flex flex-row">
            {links.map(link => {
                return (
                  <li key={link.id}>
                    <NavLink to={link.path} className={({ isActive }) => isActive ? "block py-2 px-5 text-sky-500 font-bold " : "block py-2 px-5 text-white hover:text-sky-200 hover:scale-[1.15] transition"}>{link.text}</NavLink>
                  </li>
                )
            })}
          </ul>
        </div>
        <div className="w-max flex flex-row space-x-5">
          <BiSolidBell className='fill-white my-auto' size='26'/>
          <img src={PlaceholderUser} alt="user profile" className='h-8 rounded-full border-2 border-white' />
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
