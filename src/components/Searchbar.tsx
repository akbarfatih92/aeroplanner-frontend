import { HiMagnifyingGlass } from "react-icons/hi2";

interface Props {
  id: string;
  placeholder?: string;
  value?: string;
  onChange: React.ChangeEventHandler;
}

function Searchbar( props : Props ) {
  return (
    <div className="relative flex items-center w-full h-10 rounded border-zinc-200 border focus-within:border-sky-500 bg-white overflow-hidden">
      <div className="grid place-items-center h-full w-12 text-gray-300">
        <HiMagnifyingGlass size="1.5rem" />
      </div>

      <input
        className="peer h-full w-full outline-none pr-2"
        type="text"
        id={props.id}
        value={props.value}
        placeholder={props.placeholder}
        onChange={props.onChange}
      />
    </div>
  );
}
export default Searchbar;
