export interface User {
    id: number;
    username: string;
    email: string;
    password: string;
    role: number;
    create_time: Date | null;
}

export interface UserSimple {
    id: number;
    username: string;
    email: string;
}

export interface Airport {
    id: number;
    code: string;
    name: string;
}

export interface Aircraft {
    id: number;
    code: string;
    name: string;
}

export interface FlightType {
    id: number;
    title: string;
}

export interface FlightPlan {
    user: UserSimple;
    number: number;
    status: number;
    origin: Airport;
    dest: Airport;
    originDateTime : Date;
    destDateTime: Date;
    aircraft: Aircraft;
    flightType: FlightType;
    flightLevel: number;
    cruiseSpeed: number;
    flightNumber: number | null;
    flightRule: number;
    duration: number;
    notes: string | null;
  }