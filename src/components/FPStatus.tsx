import { status } from "./Enums";

interface Props {
    statusCode: number
}

function FPStatus (props: Props) {
    const style = {
        backgroundColor: status[props.statusCode].bgColor,
      };

    return(
        <span className="py-1 px-2 rounded-sm text-sm font-semibold text-center text-white" style={style}>{status[props.statusCode].text}</span>
    )
}

export default FPStatus;