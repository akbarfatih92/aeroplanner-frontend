import { useEffect, useState } from "react";
import { status } from "./Enums";
import FPStatus from "./FPStatus";
import { FlightPlan } from "./Entities";
import FlightPlanApi from "../api/FlightPlanApi";
import { Link } from "react-router-dom";

interface Props {
  itemsPerPage: number;
  query: string;
  userId?: number;
}

function FormatDuration(duration: number): string {
  const hours = Math.floor(duration / 60);
  const minutes = duration % 60;
  return `${hours > 0 ? hours + "H" : ""} ${minutes}M`;
}

function FormatDate(startDate: Date): string {
  return `${startDate.getDate()}/${
    startDate.getMonth() + 1
  }/${startDate.getFullYear()}`;
}

function GetTimezoneAbbreviation(date: Date): string {
  return date
    .toLocaleTimeString("en-us", { timeZoneName: "short" })
    .split(" ")[2];
}

function FormatTimePeriod(start: Date, end: Date): string {
  return `${start.getHours()}:${start.getMinutes()} - ${end.getHours()}:${end.getMinutes()} ${GetTimezoneAbbreviation(
    start
  )}`;
}

function Table(props: Props) {
  const [currentPage, setCurrentPage] = useState(1);
  const [flightPlans, setFlightPlans] = useState<Array<FlightPlan>>([]);

  const refreshFlightPlans = async (userId: number | undefined) => {
    if (userId == undefined) {
      FlightPlanApi.getAllFlightPlan()
        .then((result) => setFlightPlans(result.data.flightPlans))
        .catch((err) => console.error(err));
    } else {
      FlightPlanApi.getAllflightPlanOfAUser(userId)
        .then((result) => setFlightPlans(result.data.flightPlans))
        .catch((err) => console.error(err));
    }
  };

  useEffect(() => {
    refreshFlightPlans(props.userId);
  }, []);

  const filteredData = flightPlans.filter(
    (data) =>
      data.number.toString().includes(props.query) ||
      status[data.status].text.toLowerCase().includes(props.query) ||
      data.origin.code.toLowerCase().includes(props.query) ||
      data.dest.code.toLowerCase().includes(props.query) ||
      data.flightType.title.toLowerCase().trim().includes(props.query)
  );

  const lastItemIndex = currentPage * props.itemsPerPage;
  const firstItemIndex = lastItemIndex - props.itemsPerPage;
  const currentItems = filteredData.slice(firstItemIndex, lastItemIndex);
  const totalPages = Math.ceil(filteredData.length / props.itemsPerPage);

  const handlePrevious = () => {
    setCurrentPage((prev) => (prev > 1 ? prev - 1 : prev));
  };

  const handleNext = () => {
    setCurrentPage((prev) => (prev < totalPages ? prev + 1 : prev));
  };

  const handleDelete = (userId: number, number: number) => {
    console.log("inside the button");
    if (
      confirm(
        `Are you sure you want to delete flight plans ${userId}-${number}?`
      ) == true
    ) {
      FlightPlanApi.deleteFlightPlan(userId, number)
        .then(() => refreshFlightPlans(props.userId))
        .catch((err) => console.error(err));
    }
  };

  return (
    <div>
      {/* Data Table */}
      <div className="overflow-x-auto px-6">
        <table className="min-w-full divide-y divide-gray-200 table-auto ">
          <thead className="bg-zinc-600 text-white">
            <tr className="[&>*]:py-1 [&>*]:border-l-2 [&>*]:border-white">
              <th>NO.</th>
              <th>STATUS</th>
              <th>ROUTE</th>
              <th>DATE</th>
              <th>TIME PERIOD</th>
              <th>DURATION</th>
              <th>TYPE</th>
              <th>ACTIONS</th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200 text-center">
            {currentItems.map((row, index) => (
              <tr
                key={index}
                className="p-2 even:bg-gray-50 odd:bg-white [&>*]:py-1"
              >
                <td>{row.number}</td>
                <td>
                  <FPStatus statusCode={row.status} />
                </td>
                <td>
                  {row.origin.code} ➔ {row.dest.code}
                </td>
                <td>{FormatDate(new Date(row.originDateTime))}</td>
                <td>
                  {FormatTimePeriod(
                    new Date(row.originDateTime),
                    new Date(row.destDateTime)
                  )}
                </td>
                <td>{FormatDuration(row.duration)}</td>
                <td>{row.flightType.title}</td>
                <td className="space-x-2">
                  <Link to={`/flightplan/${row.user.id}/${row.number}`}>
                    <button className="py-1 px-2 rounded-sm text-sm font-semibold text-center text-white bg-sky-500 hover:bg-sky-600 active:bg-sky-400">
                      View Details
                    </button>
                  </Link>
                  <button
                    onClick={() => handleDelete(row.user.id, row.number)}
                    className="py-1 px-2 rounded-sm text-sm font-semibold text-center text-white bg-red-500 hover:bg-red-600 active:bg-red-400"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {/* Pagination Controls */}
      <div className="flex justify-center items-center py-4">
        <button
          onClick={handlePrevious}
          disabled={currentPage === 1}
          className="mx-2"
        >
          Previous
        </button>

        <span className="mx-2">
          Page {currentPage} of {totalPages}
        </span>
        <button
          onClick={handleNext}
          disabled={currentPage === totalPages}
          className="mx-2"
        >
          Next
        </button>
      </div>
    </div>
  );
}

export default Table;
