export const status = [
    {
        bgColor: "#6b7280",
        text: "UNDEFINED"
    },
    {
        bgColor: "#FE9D0A",
        text: "PENDING"
    },
    {
        bgColor: "#01C543",
        text: "APPROVED"
    },
    {
        bgColor: "#FA1515",
        text: "DECLINED"
    },
    {
        bgColor: "#0EA5E9",
        text: "ACTIVE"
    },
    {
        bgColor: "#B1B1B1",
        text: "FINISHED"
    },
    {
        bgColor: "#950000",
        text: "CANCELLED"
    },
];

export const flightRule = [
    {
        code: "VFR",
        full: "Visual Flight Rule"
    },
    {
        code: "IFR",
        full: "Instrument Flight Rule"
    }
]

export enum UserRole {
    Admin,
    Pilot,
    ATC
}