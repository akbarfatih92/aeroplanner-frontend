import {Link} from 'react-router-dom';
import LogoBanner from '../assets/logo_banner.png';

function Footer() {
    const links = [
        {
          id: 1,
          path: "/",
          text: "Dashboard",
        },
        {
          id: 2,
          path: "/flightplans",
          text: "Flight Plans",
        },
        {
          id: 3,
          path: "/chat",
          text: "Chat With ATC",
        },
        {
          id: 4,
          path: "/help",
          text: "Help",
        },
      ];

    return(
        <footer className="shadow bg-zinc-700 mt-auto">
            <div className="w-full mx-auto p-4 pt-6">
                <ul className="flex flex-wrap items-center justify-center text-sm font-medium text-zinc-200 mx-auto">
                    {links.map(link => {
                        return (
                            <li key={link.id} className='mx-4'>
                                <Link to={link.path} className="hover:underline">{link.text}</Link>
                            </li>
                        )
                    })}
                </ul>
                <hr className="mt-5 my-2 border-zinc-200" />
                <div className="sm:flex sm:items-center sm:justify-between">
                    <Link to="/" className="flex items-center mb-4 sm:mb-0">
                        <img src={LogoBanner} className="h-8 mr-3" alt="Flowbite Logo" />
                    </Link>
                    <span className="block text-sm text-gray-500 dark:text-gray-400">Copyright© 2023 Acme Corporation. All rights reserved. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                </div>
            </div>
        </footer>
    )
}

export default Footer;