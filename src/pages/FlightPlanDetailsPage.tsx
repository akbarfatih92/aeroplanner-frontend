import { useEffect, useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";

import PageHeader from "../components/PageHeader";

import DetailsBg from "../assets/details_bg.jpg";
import { BsFillInfoSquareFill } from "react-icons/bs";
import { useParams } from "react-router-dom";
import { FlightPlan } from "../components/Entities";
import FlightPlanApi from "../api/FlightPlanApi";
import FPStatus from "../components/FPStatus";
import { flightRule } from "../components/Enums";

function FormatDate(startDate: Date): string {
  return `${startDate.getDate()} ${startDate.toLocaleString("default", {
    month: "long",
  })}, ${startDate.getFullYear()}`;
}

function GetTimezoneAbbreviation(date: Date): string {
  return date
    .toLocaleTimeString("en-us", { timeZoneName: "short" })
    .split(" ")[2];
}

function FormatTime(date: Date): string {
  return `${date.getHours()}:${date.getMinutes()} ${GetTimezoneAbbreviation(
    date
  )}`;
}

function FormatDuration(duration: number): string {
  const hours = Math.floor(duration / 60);
  const minutes = duration % 60;
  return `${hours > 0 ? hours + "H" : ""} ${minutes}M`;
}

function FlightPlanDetailsPage() {
  const [flightPlan, setFlightPlan] = useState<FlightPlan>();

  let { userId, number } = useParams();

  const refreshFlightPlan = async (userId: number, number: number) => {
    FlightPlanApi.getAFlightPlan(userId, number)
      .then((result) => setFlightPlan(result.data))
      .catch((err) => console.error(err));
  };

  useEffect(() => {
    refreshFlightPlan(Number(userId), Number(number));
  }, []);

  if (flightPlan == undefined) {
    return (
      <div className="flex flex-col min-h-screen">
        <Navbar />
        <PageHeader
          title="Flight Plan Details"
          icon={BsFillInfoSquareFill}
          bgImage={DetailsBg}
        />
        <div className="flex flex-col justify-center w-[750px] mx-auto my-auto">
          <h1 className="text-9xl">LOADING</h1>
        </div>
        <Footer />
      </div>
    );
  } else {
    return (
      <div className="flex flex-col min-h-screen">
        <Navbar />
        <PageHeader
          title="Flight Plan Details"
          icon={BsFillInfoSquareFill}
          bgImage={DetailsBg}
        />
        <div className="flex flex-col w-[750px] mx-auto my-2">
          <div className="relative w-full">
            <h1 className="absolute inset-y-0 left-0">#{flightPlan.number}</h1>
            <p className="absolute inset-0 w-full text-center">
              {FormatDate(new Date(flightPlan.originDateTime))}
            </p>
            <div className="absolute inset-y-0 right-0">
              <FPStatus statusCode={flightPlan.status} />
            </div>
          </div>
          <div className="mt-10 mb-2 flex flex-row justify-center items-center space-x-10 text-center">
            <div className="flex flex-col items-center">
              <b className="text-7xl font-bold">{flightPlan.origin.code}</b>
              {flightPlan.origin.name}
            </div>
            <b className="text-7xl font-bold">➔</b>
            <div className="flex flex-col items-center">
              <b className="text-7xl font-bold">{flightPlan.dest.code}</b>
              {flightPlan.dest.name}
            </div>
          </div>
          <div className="grid grid-cols-[1fr_4fr_1fr] w-3/4 mx-auto">
            <span className="text-sm">
              {FormatTime(new Date(flightPlan.originDateTime))}
            </span>
            <div className="flex flex-row items-center w-full">
              <div className="circle bg-sky-500 rounded-full w-3 h-3"></div>
              <div className="w-full relative bg-sky-500 h-1">
                <span className="absolute top-1/2 left-1/2 bg-white px-2 z-10 -translate-x-1/2 -translate-y-1/2 text-[0.75rem]">{FormatDuration(flightPlan.duration)}</span>
              </div>
              <div className="circle bg-sky-500 rounded-full w-3 h-3"></div>
            </div>
            <span className="text-sm text-right">
              {FormatTime(new Date(flightPlan.destDateTime))}
            </span>
          </div>
          <hr className="h-px my-6 bg-gray-100 border-0 w-full" />
          <div className="grid grid-cols-[2fr_3fr_2fr_3fr] grid-rows-5 gap-x-2">
            <h2 className="text-2xl font-semibold col-span-4">Details</h2>
            <div className="flex flex-row font-semibold">
                <p className="flex-grow">Flight Number</p>
                <p>:</p>
            </div>
            <p>{flightPlan.flightNumber == undefined ? "N/A" : flightPlan.flightNumber}</p>

            <div className="flex flex-row font-semibold">
                <p className="flex-grow">Flight Rule</p>
                <p>:</p>
            </div>
            <p>{flightRule[flightPlan.flightRule].code}</p>

            <div className="flex flex-row font-semibold">
                <p className="flex-grow">Registration</p>
                <p>:</p>
            </div>
            <p>{flightPlan.flightNumber == undefined ? "N/A" : flightPlan.flightNumber}</p>

            <div className="flex flex-row font-semibold">
                <p className="flex-grow">Aircraft</p>
                <p>:</p>
            </div>
            <p>{flightPlan.aircraft.name}</p>

            <div className="flex flex-row font-semibold">
                <p className="flex-grow">Flight Level</p>
                <p>:</p>
            </div>
            <p>FL {flightPlan.flightLevel}</p>

            <div className="flex flex-row font-semibold">
                <p className="flex-grow">Cruise Speed</p>
                <p>:</p>
            </div>
            <p>{flightPlan.cruiseSpeed} Knots</p>

            <div className="flex flex-row font-semibold">
                <p className="flex-grow">Flight type</p>
                <p>:</p>
            </div>
            <p className="col-span-3">{flightPlan.flightType.title}</p>

          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default FlightPlanDetailsPage;
