//import React from "react"
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";

function ChatPage() {
  return (
    <div className='flex flex-col min-h-screen'>
      <Navbar/>
      <div className="m-auto justify-center text-center">
        <h1 className="text-9xl font-bold">Chat/Messaging Page</h1>
        <p className="mt-10">Under construction, please come again next time!</p>
      </div>
      <Footer/>
    </div>
  )
}

export default ChatPage;