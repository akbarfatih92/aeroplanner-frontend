import { useState } from "react";

import Navbar from "../components/Navbar";
import Footer from "../components/Footer";

import PageHeader from "../components/PageHeader";
import Searchbar from "../components/Searchbar";
import Table from "../components/Table";

import DefaultBg from "../assets/Aeroplanner-bg-1.png";
import { HiMiniSquare3Stack3D } from "react-icons/hi2";
import { useParams } from "react-router-dom";

function FlightPlanPage() {
  let {userId} = useParams();

  const [query, setQuery] = useState<string>("");

  const HandleSearchOnChange = (e: React.ChangeEvent<any>) => {
    setQuery(e.target.value.trim().toLowerCase());
  };

  return (
    <div className="flex flex-col min-h-screen">
      <Navbar />
      <PageHeader
        title="Flight Plans"
        icon={HiMiniSquare3Stack3D}
        bgImage={DefaultBg}
      />
      <div className="w-full p-4 flex flex-row space-x-5 bg-white sticky top-[56px] z-20">
        <Searchbar
          id="search"
          placeholder="Search created flight plans"
          onChange={HandleSearchOnChange}
        />
        <button className="bg-sky-500 hover:bg-sky-700 text-white font-bold py-2 px-6 rounded whitespace-nowrap">
          Create Flight Plan
        </button>
      </div>
      <Table itemsPerPage={20} query={query} userId={userId == undefined ? undefined : Number(userId)}/>
      <Footer />
    </div>
  );
}

export default FlightPlanPage;
